
%{
#include<stdio.h>

int pos = 0;
char var[10];

void yyerror(const char *str) {
    fprintf(stderr,"error: %s\n",str);
    exit(-1);
}
 
int yywrap() {
        return 1;
} 
  
main() {
        yyparse();
} 
%}

%union{
    char *str;
}
%token SELECT
%token PREFIX
%token WHERE
%token OPECURBRA /* Open curly braces */
%token CLOCURBRA /* Close curly braces */
%token <str> URI
%token <str> VAR
%token DOT
%token OPENPARENTHESIS
%token CLOPARENTESIS
%token OPEPTBRA
%token CLOPTBRA
%token COLON
%%

sparql : query
       | prefixes query
       ;
prefixes : prefix
         | prefixes prefix
         ;
prefix   : PREFIX URI COLON OPEPTBRA tripats CLOPTBRA
         /*| /* empty */
         ;
query    : SELECT projections filters
         ;
projections: VAR {printf("adiciona variavel %s nas projeções. \n", $1);}
           | VAR projections {printf("Adiciona variavel %s nas projeções", $1);}
           ;
tripats  : tripat
         | tripats tripat
         ;

tripat   : URI URI URI DOT
         | VAR URI URI DOT {printf("%s %s %s", $1, $2, $3);}
         | URI URI VAR DOT
         | VAR URI VAR DOT
         ;
filters  : /* empty */
         | WHERE OPECURBRA tripats CLOCURBRA
         ;

%%

