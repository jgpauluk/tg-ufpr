import mysql.connector

class ConnectDb:
    def executeQuery(self, qry):
        self.cursor = self.getConnection().cursor(dictionary=True)
        self.cursor.execute(qry)
        return self.cursor

    def executeQueryFile(self, path, *args):
        f = open(path, 'r')
        qry =  f.read()
        if len(args) > 0:
            qry = qry.format(*args)
        return self.executeQuery(qry)
    
    def getConnection(self):
        #if cls.con is None:
        self.con = mysql.connector.connect(user='tg', password='123', database='TG_TESTE2')
        return self.con
        #return cls.con

#cnx = mysql.connector.connect(user='tg', password='123', database='TG_TESTE1')
#cursor = cnx.cursor()

#query = ('SELECT TP_SUBJ, NM_TABLE FROM SCHEMA_INFO;')

#ConnectDb.executeQuery(query)
#cursor.execute(query)

#for (TP_SUBJ, NM_TABLE) in cursor:
#    print TP_SUBJ, NM_TABLE
