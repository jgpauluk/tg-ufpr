class ColumnInfo:
    def __init__(self, columnName, predicate, overflow):
        self.columnName = columnName
        self.predicate = predicate
        if overflow == 0:
            self.id_overflow = False
        else:
            self.id_overflow = True
