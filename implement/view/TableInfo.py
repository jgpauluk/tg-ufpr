from connectDb import ConnectDb
from ColumnInfo import ColumnInfo

class TableInfo:
    def __init__(self, tableName):
        self.tableName = tableName
        self.columns = list()

        ## procura colunas da tabela
        db = ConnectDb()
        for row in db.executeQueryFile('queries/qry2.sql', tableName):
            self.columns.append(ColumnInfo(row['columnName'], row['predicate'], row['overflow']))
        #print self.columns

    # SELECT proj FROM tab WHERE cond
    def getViewSQL(self):
        proj = ''
        tab = ''
        cond = ''
        primTab = ''
        for c in self.columns:
            if len(proj) > 0:
                proj += ', \n'
 
            # Se tabela nao possui registro no overflow, somente projeta
            if not c.id_overflow:
                proj += '  NULL AS ' + c.columnName
                continue
            # Caso contrario, faz a juncao
            else:
                #propara projecao
                proj += '  ' + c.columnName + '.subj' + ' AS ' + c.columnName
                
                #se for a primeira tabela, insere condicoes no WHERE
                if len(tab) == 0:
                    proj = '  Overflow_' + self.tableName + '.subj AS OID\n' 
                    tab = '  Overflow_' + self.tableName + ' AS ' + c.columnName + '\n'
                    primTab = c.columnName
                    cond = '  ' + c.columnName + '.' + 'subj = ' + '"' + c.predicate + '"'
                else:
                    #caso contrario, monta os joins
                    tab += 'LEFT JOIN ' + 'Overflow_' + self.tableName + ' AS ' + c.columnName + ' ON\n'
                    tab += '  ' + primTab + '.subj = ' + c.columnName + '.subj\n'

        if len(tab) != 0:
            tab = '\nFROM\n' + tab
        #se todas as tabelas forem nulas, projeta OID coomo NULL
        else:
            proj += ',\n  NULL AS OID'
        if len(cond) != 0:
            cond = 'WHERE\n' + cond
        return 'CREATE OR REPLACE VIEW ' + self.tableName + '_View ' + 'AS\nSELECT\n' + proj + tab + cond + ';'

            

    def printTableInfo(self):
        print self.tableName
        for col in self.columns:
            print '  ', col.columnName, ' - ', col.predicate, col.id_overflow
