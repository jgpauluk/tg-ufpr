SELECT
    fp.columnName,
    fp.predicate,
    CASE
        WHEN EXISTS(
            SELECT
                1
            FROM
                TB_DatabaseSchema AS b
            WHERE
                b.cs_identifier = ds.cs_identifier
            AND b.property = ds.property
            AND b.tableName like 'Overflow_%') THEN 1
        ELSE 0
    END AS overflow
FROM
    TB_FullPredicate fp
    INNER JOIN TB_DatabaseSchema ds ON
        ds.tableName = fp.tableName
    AND ds.tableAttribute = fp.columnName
WHERE
    fp.tableName = '{0}'
