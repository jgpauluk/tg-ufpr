import sys
import connectDb
from TableInfo import TableInfo


class createView:
    def montaSQL(self):
        for t in self.tables:
            #t.printTableInfo()
            print t.getViewSQL()
            pass
    ## Etapa 1 - Recupera as tabelas
    ## Esta etapa recupera todas as tabelas principais, isto eh, todos os que possuem o atributo 'OID'
    def recuperaTabelas(self):
        self.tables = list()
        #or r in connectDb.ConnectDb.executeQueryFile('queries/qry2.sql', 'TrackRDF'):
        #   print r
        #qry = open('queries/qry1.sql', 'r').read() #'SELECT tableName FROM TB_DatabaseSchema WHERE tableAttribute = \'OID\''

        db = connectDb.ConnectDb()
        for row in db.executeQueryFile('queries/qry1.sql'):
            self.tables.append(TableInfo(row['tableName']))
        
        #for tab in tables:
        #    tab.printTableInfo()
        self.montaSQL()

    
cv = createView()
cv.recuperaTabelas()


#def main(argv):
#    print 'a'

#if __name__ == 'main':
#    main(sys.argv)
