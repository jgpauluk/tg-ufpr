
%option caseless
%{
    #include "parser.tab.h"
    int line = 0;
    extern YYSTYPE yylval;
%}
%%
"SELECT" {return SELECT;}
"PREFIX" {return PREFIX;}
"WHERE" {return WHERE;}
"{" {return OPECURBRA;}
"}" {return CLOCURBRA;}
[a-zA-Z]+ {yylval.str = strdup(yytext); return URI;}
\?[a-zA-Z]+ {yylval.str = strdup(yytext); return VAR;}
"." {return DOT;}
[\n] {line++;}
[ \t]+
. {printf("Unrecognizable character '%s' at line %d", yytext, line); exit(1);}
%%
