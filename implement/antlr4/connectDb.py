import mysql.connector

class ConnectDb(object):
    con = None

    @classmethod
    def executeQuery(cls, qry):
        cursor = cls.getConnection().cursor(dictionary=True)
        cursor.execute(qry)
        return cursor

    @classmethod
    def getConnection(cls):
        if cls.con is None:
            cls.con = mysql.connector.connect(user='tg', password='123', database='TG_TESTE2')
        return cls.con

#cnx = mysql.connector.connect(user='tg', password='123', database='TG_TESTE1')
#cursor = cnx.cursor()

#query = ('SELECT TP_SUBJ, NM_TABLE FROM SCHEMA_INFO;')

#ConnectDb.executeQuery(query)
#cursor.execute(query)

#for (TP_SUBJ, NM_TABLE) in cursor:
#    print TP_SUBJ, NM_TABLE
