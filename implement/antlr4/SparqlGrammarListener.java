// Generated from SparqlGrammar.g4 by ANTLR 4.5
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SparqlGrammarParser}.
 */
public interface SparqlGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SparqlGrammarParser#sparql}.
	 * @param ctx the parse tree
	 */
	void enterSparql(SparqlGrammarParser.SparqlContext ctx);
	/**
	 * Exit a parse tree produced by {@link SparqlGrammarParser#sparql}.
	 * @param ctx the parse tree
	 */
	void exitSparql(SparqlGrammarParser.SparqlContext ctx);
	/**
	 * Enter a parse tree produced by {@link SparqlGrammarParser#prefixes}.
	 * @param ctx the parse tree
	 */
	void enterPrefixes(SparqlGrammarParser.PrefixesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SparqlGrammarParser#prefixes}.
	 * @param ctx the parse tree
	 */
	void exitPrefixes(SparqlGrammarParser.PrefixesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SparqlGrammarParser#prefix}.
	 * @param ctx the parse tree
	 */
	void enterPrefix(SparqlGrammarParser.PrefixContext ctx);
	/**
	 * Exit a parse tree produced by {@link SparqlGrammarParser#prefix}.
	 * @param ctx the parse tree
	 */
	void exitPrefix(SparqlGrammarParser.PrefixContext ctx);
	/**
	 * Enter a parse tree produced by {@link SparqlGrammarParser#query}.
	 * @param ctx the parse tree
	 */
	void enterQuery(SparqlGrammarParser.QueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link SparqlGrammarParser#query}.
	 * @param ctx the parse tree
	 */
	void exitQuery(SparqlGrammarParser.QueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link SparqlGrammarParser#projections}.
	 * @param ctx the parse tree
	 */
	void enterProjections(SparqlGrammarParser.ProjectionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SparqlGrammarParser#projections}.
	 * @param ctx the parse tree
	 */
	void exitProjections(SparqlGrammarParser.ProjectionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SparqlGrammarParser#tripats}.
	 * @param ctx the parse tree
	 */
	void enterTripats(SparqlGrammarParser.TripatsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SparqlGrammarParser#tripats}.
	 * @param ctx the parse tree
	 */
	void exitTripats(SparqlGrammarParser.TripatsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SparqlGrammarParser#tripat}.
	 * @param ctx the parse tree
	 */
	void enterTripat(SparqlGrammarParser.TripatContext ctx);
	/**
	 * Exit a parse tree produced by {@link SparqlGrammarParser#tripat}.
	 * @param ctx the parse tree
	 */
	void exitTripat(SparqlGrammarParser.TripatContext ctx);
	/**
	 * Enter a parse tree produced by {@link SparqlGrammarParser#subj}.
	 * @param ctx the parse tree
	 */
	void enterSubj(SparqlGrammarParser.SubjContext ctx);
	/**
	 * Exit a parse tree produced by {@link SparqlGrammarParser#subj}.
	 * @param ctx the parse tree
	 */
	void exitSubj(SparqlGrammarParser.SubjContext ctx);
	/**
	 * Enter a parse tree produced by {@link SparqlGrammarParser#pred}.
	 * @param ctx the parse tree
	 */
	void enterPred(SparqlGrammarParser.PredContext ctx);
	/**
	 * Exit a parse tree produced by {@link SparqlGrammarParser#pred}.
	 * @param ctx the parse tree
	 */
	void exitPred(SparqlGrammarParser.PredContext ctx);
	/**
	 * Enter a parse tree produced by {@link SparqlGrammarParser#obj}.
	 * @param ctx the parse tree
	 */
	void enterObj(SparqlGrammarParser.ObjContext ctx);
	/**
	 * Exit a parse tree produced by {@link SparqlGrammarParser#obj}.
	 * @param ctx the parse tree
	 */
	void exitObj(SparqlGrammarParser.ObjContext ctx);
	/**
	 * Enter a parse tree produced by {@link SparqlGrammarParser#filters}.
	 * @param ctx the parse tree
	 */
	void enterFilters(SparqlGrammarParser.FiltersContext ctx);
	/**
	 * Exit a parse tree produced by {@link SparqlGrammarParser#filters}.
	 * @param ctx the parse tree
	 */
	void exitFilters(SparqlGrammarParser.FiltersContext ctx);
}