# Generated from java-escape by ANTLR 4.5
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
package = globals().get("__package__", None)
ischild = len(package)>0 if package is not None else False
if ischild:
    from .SparqlGrammarListener import SparqlGrammarListener
else:
    from SparqlGrammarListener import SparqlGrammarListener
def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3")
        buf.write(u"\16J\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write(u"\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\3\2\3\2\3")
        buf.write(u"\2\3\3\7\3\35\n\3\f\3\16\3 \13\3\3\4\3\4\3\4\3\4\3\4")
        buf.write(u"\3\4\3\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\5\6\60\n\6\3\7\6")
        buf.write(u"\7\63\n\7\r\7\16\7\64\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\n")
        buf.write(u"\3\n\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\5\fH\n\f\3\f\2")
        buf.write(u"\2\r\2\4\6\b\n\f\16\20\22\24\26\2\3\3\2\13\fB\2\30\3")
        buf.write(u"\2\2\2\4\36\3\2\2\2\6!\3\2\2\2\b(\3\2\2\2\n/\3\2\2\2")
        buf.write(u"\f\62\3\2\2\2\16\66\3\2\2\2\20;\3\2\2\2\22=\3\2\2\2\24")
        buf.write(u"?\3\2\2\2\26G\3\2\2\2\30\31\5\4\3\2\31\32\5\b\5\2\32")
        buf.write(u"\3\3\2\2\2\33\35\5\6\4\2\34\33\3\2\2\2\35 \3\2\2\2\36")
        buf.write(u"\34\3\2\2\2\36\37\3\2\2\2\37\5\3\2\2\2 \36\3\2\2\2!\"")
        buf.write(u"\7\4\2\2\"#\7\13\2\2#$\7\b\2\2$%\7\t\2\2%&\5\f\7\2&\'")
        buf.write(u"\7\n\2\2\'\7\3\2\2\2()\7\3\2\2)*\5\n\6\2*+\5\26\f\2+")
        buf.write(u"\t\3\2\2\2,\60\7\f\2\2-.\7\f\2\2.\60\5\n\6\2/,\3\2\2")
        buf.write(u"\2/-\3\2\2\2\60\13\3\2\2\2\61\63\5\16\b\2\62\61\3\2\2")
        buf.write(u"\2\63\64\3\2\2\2\64\62\3\2\2\2\64\65\3\2\2\2\65\r\3\2")
        buf.write(u"\2\2\66\67\5\20\t\2\678\5\22\n\289\5\24\13\29:\7\r\2")
        buf.write(u"\2:\17\3\2\2\2;<\t\2\2\2<\21\3\2\2\2=>\7\13\2\2>\23\3")
        buf.write(u"\2\2\2?@\t\2\2\2@\25\3\2\2\2AH\3\2\2\2BC\7\5\2\2CD\7")
        buf.write(u"\6\2\2DE\5\f\7\2EF\7\7\2\2FH\3\2\2\2GA\3\2\2\2GB\3\2")
        buf.write(u"\2\2H\27\3\2\2\2\6\36/\64G")
        return buf.getvalue()


class SparqlGrammarParser ( Parser ):

    grammarFileName = "java-escape"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ u"<INVALID>", u"'SELECT'", u"'PREFIX'", u"'WHERE'", 
                     u"'{'", u"'}'", u"':'", u"'<'", u"'>'", u"<INVALID>", 
                     u"<INVALID>", u"'.'" ]

    symbolicNames = [ u"<INVALID>", u"SELECT", u"PREFIX", u"WHERE", u"OPECURBRA", 
                      u"CLOCURBRA", u"COLON", u"OPEPTBRA", u"CLOPTBRA", 
                      u"URI", u"VAR", u"DOT", u"SPACES" ]

    RULE_sparql = 0
    RULE_prefixes = 1
    RULE_prefix = 2
    RULE_query = 3
    RULE_projections = 4
    RULE_tripats = 5
    RULE_tripat = 6
    RULE_subj = 7
    RULE_pred = 8
    RULE_obj = 9
    RULE_filters = 10

    ruleNames =  [ u"sparql", u"prefixes", u"prefix", u"query", u"projections", 
                   u"tripats", u"tripat", u"subj", u"pred", u"obj", u"filters" ]

    EOF = Token.EOF
    SELECT=1
    PREFIX=2
    WHERE=3
    OPECURBRA=4
    CLOCURBRA=5
    COLON=6
    OPEPTBRA=7
    CLOPTBRA=8
    URI=9
    VAR=10
    DOT=11
    SPACES=12

    def __init__(self, input):
        super(SparqlGrammarParser, self).__init__(input)
        self.checkVersion("4.5")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class SparqlContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SparqlGrammarParser.SparqlContext, self).__init__(parent, invokingState)
            self.parser = parser

        def prefixes(self):
            return self.getTypedRuleContext(SparqlGrammarParser.PrefixesContext,0)


        def query(self):
            return self.getTypedRuleContext(SparqlGrammarParser.QueryContext,0)


        def getRuleIndex(self):
            return SparqlGrammarParser.RULE_sparql

        def enterRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.enterSparql(self)

        def exitRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.exitSparql(self)




    def sparql(self):

        localctx = SparqlGrammarParser.SparqlContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_sparql)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 22
            self.prefixes()
            self.state = 23
            self.query()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PrefixesContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SparqlGrammarParser.PrefixesContext, self).__init__(parent, invokingState)
            self.parser = parser

        def prefix(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(SparqlGrammarParser.PrefixContext)
            else:
                return self.getTypedRuleContext(SparqlGrammarParser.PrefixContext,i)


        def getRuleIndex(self):
            return SparqlGrammarParser.RULE_prefixes

        def enterRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.enterPrefixes(self)

        def exitRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.exitPrefixes(self)




    def prefixes(self):

        localctx = SparqlGrammarParser.PrefixesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_prefixes)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 28
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==SparqlGrammarParser.PREFIX:
                self.state = 25
                self.prefix()
                self.state = 30
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PrefixContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SparqlGrammarParser.PrefixContext, self).__init__(parent, invokingState)
            self.parser = parser

        def PREFIX(self):
            return self.getToken(SparqlGrammarParser.PREFIX, 0)

        def URI(self):
            return self.getToken(SparqlGrammarParser.URI, 0)

        def COLON(self):
            return self.getToken(SparqlGrammarParser.COLON, 0)

        def OPEPTBRA(self):
            return self.getToken(SparqlGrammarParser.OPEPTBRA, 0)

        def tripats(self):
            return self.getTypedRuleContext(SparqlGrammarParser.TripatsContext,0)


        def CLOPTBRA(self):
            return self.getToken(SparqlGrammarParser.CLOPTBRA, 0)

        def getRuleIndex(self):
            return SparqlGrammarParser.RULE_prefix

        def enterRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.enterPrefix(self)

        def exitRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.exitPrefix(self)




    def prefix(self):

        localctx = SparqlGrammarParser.PrefixContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_prefix)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 31
            self.match(SparqlGrammarParser.PREFIX)
            self.state = 32
            self.match(SparqlGrammarParser.URI)
            self.state = 33
            self.match(SparqlGrammarParser.COLON)
            self.state = 34
            self.match(SparqlGrammarParser.OPEPTBRA)
            self.state = 35
            self.tripats()
            self.state = 36
            self.match(SparqlGrammarParser.CLOPTBRA)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class QueryContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SparqlGrammarParser.QueryContext, self).__init__(parent, invokingState)
            self.parser = parser

        def SELECT(self):
            return self.getToken(SparqlGrammarParser.SELECT, 0)

        def projections(self):
            return self.getTypedRuleContext(SparqlGrammarParser.ProjectionsContext,0)


        def filters(self):
            return self.getTypedRuleContext(SparqlGrammarParser.FiltersContext,0)


        def getRuleIndex(self):
            return SparqlGrammarParser.RULE_query

        def enterRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.enterQuery(self)

        def exitRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.exitQuery(self)




    def query(self):

        localctx = SparqlGrammarParser.QueryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_query)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 38
            self.match(SparqlGrammarParser.SELECT)
            self.state = 39
            self.projections()
            self.state = 40
            self.filters()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ProjectionsContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SparqlGrammarParser.ProjectionsContext, self).__init__(parent, invokingState)
            self.parser = parser

        def VAR(self):
            return self.getToken(SparqlGrammarParser.VAR, 0)

        def projections(self):
            return self.getTypedRuleContext(SparqlGrammarParser.ProjectionsContext,0)


        def getRuleIndex(self):
            return SparqlGrammarParser.RULE_projections

        def enterRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.enterProjections(self)

        def exitRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.exitProjections(self)




    def projections(self):

        localctx = SparqlGrammarParser.ProjectionsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_projections)
        try:
            self.state = 45
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 42
                self.match(SparqlGrammarParser.VAR)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 43
                self.match(SparqlGrammarParser.VAR)
                self.state = 44
                self.projections()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TripatsContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SparqlGrammarParser.TripatsContext, self).__init__(parent, invokingState)
            self.parser = parser

        def tripat(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(SparqlGrammarParser.TripatContext)
            else:
                return self.getTypedRuleContext(SparqlGrammarParser.TripatContext,i)


        def getRuleIndex(self):
            return SparqlGrammarParser.RULE_tripats

        def enterRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.enterTripats(self)

        def exitRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.exitTripats(self)




    def tripats(self):

        localctx = SparqlGrammarParser.TripatsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_tripats)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 48 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 47
                self.tripat()
                self.state = 50 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==SparqlGrammarParser.URI or _la==SparqlGrammarParser.VAR):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TripatContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SparqlGrammarParser.TripatContext, self).__init__(parent, invokingState)
            self.parser = parser

        def subj(self):
            return self.getTypedRuleContext(SparqlGrammarParser.SubjContext,0)


        def pred(self):
            return self.getTypedRuleContext(SparqlGrammarParser.PredContext,0)


        def obj(self):
            return self.getTypedRuleContext(SparqlGrammarParser.ObjContext,0)


        def DOT(self):
            return self.getToken(SparqlGrammarParser.DOT, 0)

        def getRuleIndex(self):
            return SparqlGrammarParser.RULE_tripat

        def enterRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.enterTripat(self)

        def exitRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.exitTripat(self)




    def tripat(self):

        localctx = SparqlGrammarParser.TripatContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_tripat)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 52
            self.subj()
            self.state = 53
            self.pred()
            self.state = 54
            self.obj()
            self.state = 55
            self.match(SparqlGrammarParser.DOT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SubjContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SparqlGrammarParser.SubjContext, self).__init__(parent, invokingState)
            self.parser = parser

        def URI(self):
            return self.getToken(SparqlGrammarParser.URI, 0)

        def VAR(self):
            return self.getToken(SparqlGrammarParser.VAR, 0)

        def getRuleIndex(self):
            return SparqlGrammarParser.RULE_subj

        def enterRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.enterSubj(self)

        def exitRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.exitSubj(self)




    def subj(self):

        localctx = SparqlGrammarParser.SubjContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_subj)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 57
            _la = self._input.LA(1)
            if not(_la==SparqlGrammarParser.URI or _la==SparqlGrammarParser.VAR):
                self._errHandler.recoverInline(self)
            else:
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PredContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SparqlGrammarParser.PredContext, self).__init__(parent, invokingState)
            self.parser = parser

        def URI(self):
            return self.getToken(SparqlGrammarParser.URI, 0)

        def getRuleIndex(self):
            return SparqlGrammarParser.RULE_pred

        def enterRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.enterPred(self)

        def exitRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.exitPred(self)




    def pred(self):

        localctx = SparqlGrammarParser.PredContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_pred)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 59
            self.match(SparqlGrammarParser.URI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ObjContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SparqlGrammarParser.ObjContext, self).__init__(parent, invokingState)
            self.parser = parser

        def URI(self):
            return self.getToken(SparqlGrammarParser.URI, 0)

        def VAR(self):
            return self.getToken(SparqlGrammarParser.VAR, 0)

        def getRuleIndex(self):
            return SparqlGrammarParser.RULE_obj

        def enterRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.enterObj(self)

        def exitRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.exitObj(self)




    def obj(self):

        localctx = SparqlGrammarParser.ObjContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_obj)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 61
            _la = self._input.LA(1)
            if not(_la==SparqlGrammarParser.URI or _la==SparqlGrammarParser.VAR):
                self._errHandler.recoverInline(self)
            else:
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FiltersContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(SparqlGrammarParser.FiltersContext, self).__init__(parent, invokingState)
            self.parser = parser

        def WHERE(self):
            return self.getToken(SparqlGrammarParser.WHERE, 0)

        def OPECURBRA(self):
            return self.getToken(SparqlGrammarParser.OPECURBRA, 0)

        def tripats(self):
            return self.getTypedRuleContext(SparqlGrammarParser.TripatsContext,0)


        def CLOCURBRA(self):
            return self.getToken(SparqlGrammarParser.CLOCURBRA, 0)

        def getRuleIndex(self):
            return SparqlGrammarParser.RULE_filters

        def enterRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.enterFilters(self)

        def exitRule(self, listener):
            if isinstance( listener, SparqlGrammarListener ):
                listener.exitFilters(self)




    def filters(self):

        localctx = SparqlGrammarParser.FiltersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_filters)
        try:
            self.state = 69
            token = self._input.LA(1)
            if token in [SparqlGrammarParser.EOF]:
                self.enterOuterAlt(localctx, 1)


            elif token in [SparqlGrammarParser.WHERE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 64
                self.match(SparqlGrammarParser.WHERE)
                self.state = 65
                self.match(SparqlGrammarParser.OPECURBRA)
                self.state = 66
                self.tripats()
                self.state = 67
                self.match(SparqlGrammarParser.CLOCURBRA)

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx




