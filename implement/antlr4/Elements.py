
#classe Tripat
#contem informacoes referente a uma tripla
class Tripat:
    def __init__(self, s, p, o):
        self.subj = s
        self.pred = p
        self.obj  = o

    def printTripat(self):
        print self.subj.value, ' ', self.pred.value, ' ', self.obj.value

#classe Elements
#contem os elementos de uma consulta sparql
##tripats: lista de Tripat
##projections: lista das variaveis a projetar
class Elements:
    def __init__(self):
        self.tripats = list()
        self.projections = list()

    def getPredicates(self):
        predicates = list()
        for tp in self.tripats:
            predicates.append(tp.pred.value)
        return predicates

#classe URI
##value: string da URI
class URI:
    def __init__(self, s):
        self.value = str(s)

#classe Var
##value: string contendo a variavel
class Variable:
    def __init__(self, s):
        self.value = str(s)
