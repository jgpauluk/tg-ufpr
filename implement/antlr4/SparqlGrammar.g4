grammar SparqlGrammar;

sparql : prefixes query
       ;
prefixes : prefix*
         ;
prefix   : PREFIX URI COLON OPEPTBRA tripats CLOPTBRA
         ;
query    : SELECT projections filters
         ;
projections: VAR 
           | VAR projections
           ;
tripats  : tripat+
         ;

tripat   : subj pred obj DOT
         ;

subj     : URI
         | VAR
         ;

pred     : URI
         ;

obj      : URI
         | VAR
         ;

filters  : /* empty */
         | WHERE OPECURBRA tripats CLOCURBRA
         ;

SELECT: 'SELECT';
PREFIX: 'PREFIX';
WHERE : 'WHERE';
OPECURBRA: '{';
CLOCURBRA: '}';
COLON: ':';
OPEPTBRA: '<';
CLOPTBRA: '>';
URI: [a-zA-Z_]+;
VAR: '?'[a-zA-Z];
DOT: '.';
SPACES: [ \t\n]+ -> skip;

