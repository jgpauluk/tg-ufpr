USE TG_TESTE1;
GO

CREATE TABLE SCHEMA_INFO (
  TP_SUBJ   VARCHAR(5) ,
  ID_PRED   NUMERIC(10),
  TP_OBJ    VARCHAR(5) ,
  NM_TABLE  VARCHAR(20),
  NM_OBJATR VARCHAR(20)
);

INSERT INTO SCHEMA_INFO
VALUES ('cs1', 1, 'lit', 'pessoaX', 'nome');

INSERT INTO SCHEMA_INFO
VALUES ('cs1', 2, 'cs3', 'pessoaX', 'end');

INSERT INTO SCHEMA_INFO
VALUES ('cs2', 1, 'lit', 'pessoa', 'nome');

INSERT INTO SCHEMA_INFO
VALUES ('cs2', 2, 'cs3', 'pessoa', 'end');

INSERT INTO SCHEMA_INFO
VALUES ('cs2', 3, 'lit', 'pessoa', 'tel');

INSERT INTO SCHEMA_INFO
VALUES ('cs2', 4, 'cs4', 'pessoa', 'celular');

INSERT INTO SCHEMA_INFO
VALUES ('cs3', 5, 'lit', 'ends', 'rua');

INSERT INTO SCHEMA_INFO
VALUES ('cs4', 6, 'lit', 'catCelular', 'modelo');

INSERT INTO SCHEMA_INFO
VALUES ('cs4', 7, 'lit', 'catCelular', 'produzido');






