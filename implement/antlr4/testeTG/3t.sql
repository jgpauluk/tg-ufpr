SELECT SQL_NO_CACHE
    title.obj, typeX.obj, labelX.obj
FROM 
    RDF_Tripla AS title 
    INNER JOIN RDF_Tripla typeX ON
        typeX.pred = '<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>'
    AND typeX.subj = title.subj
    INNER JOIN RDF_Tripla labelX ON
        labelX.pred = '<http://www.w3.org/2000/01/rdf-schema#label>'
    AND labelX.subj = title.subj
WHERE
    title.pred = '<http://purl.org/dc/elements/1.1/title>';
