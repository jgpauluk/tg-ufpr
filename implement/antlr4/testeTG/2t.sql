SELECT SQL_NO_CACHE
    published.obj, typeP.obj, labelP.obj
FROM 
    RDF_Tripla AS published 
    INNER JOIN RDF_Tripla typeP ON
        typeP.pred = '<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>'
    AND typeP.subj = published.subj
    INNER JOIN RDF_Tripla labelP ON
        labelP.pred = '<http://www.w3.org/2000/01/rdf-schema#label>'
    AND labelP.subj = published.subj
WHERE
    published.pred = '<http://purl.org/ontology/mo/published_as>';
