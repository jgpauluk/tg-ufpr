SELECT SQL_NO_CACHE
 a.fk_produced_signal2921H7 AS s,
a.place6S18HL AS p,
a.OID AS a
 FROM
 (SELECT RecordingRDF.OID, RecordingRDF.place AS place6S18HL, RecordingRDF.fk_produced_signal AS fk_produced_signal2921H7, RecordingRDF.type AS type5QR6KC FROM RecordingRDF WHERE RecordingRDF.place IS NOT NULL AND RecordingRDF.fk_produced_signal IS NOT NULL AND RecordingRDF.type IS NOT NULL) a;
