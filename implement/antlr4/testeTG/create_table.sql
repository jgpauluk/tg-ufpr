SELECT
    subj.OID, st.pred, obj.OID
FROM
    RDF_Tripla st
    INNER JOIN TB_Subj_OID subj ON
        subj.subj = st.subj;
    INNER JOIN TB_Subj_OID obj ON
         obj.subj = st.obj;
