select
 a.OID, a.place
from
 RecordingRDF a,
 SignalRDF s1, labelMultivalueRDF s2
where
  s1.OID = s2.OID and
  s1.OID = a.fk_produced_signal
