SELECT SQL_NO_CACHE
 s.label935S6X AS l,
s.typeRFH7DY AS y,
s.fk_published_asOJMHLF AS x,
s.OID AS s
 FROM
 (SELECT SignalRDF.OID, SignalRDF.fk_published_as AS fk_published_asOJMHLF, SignalRDF.type AS typeRFH7DY, labelMultivalueRDF.label AS label935S6X FROM SignalRDF, labelMultivalueRDF WHERE SignalRDF.fk_published_as IS NOT NULL AND SignalRDF.type IS NOT NULL AND labelMultivalueRDF.OID = SignalRDF.OID AND labelMultivalueRDF.label IS NOT NULL) s;
