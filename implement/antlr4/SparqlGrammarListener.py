# Generated from java-escape by ANTLR 4.5
from antlr4 import *
from Elements import *

# This class defines a complete listener for a parse tree produced by SparqlGrammarParser.
class SparqlGrammarListener(ParseTreeListener):
    def __init__(self):
        self.elements = Elements()

    # Enter a parse tree produced by SparqlGrammarParser#sparql.
    def enterSparql(self, ctx):
        pass

    # Exit a parse tree produced by SparqlGrammarParser#sparql.
    def exitSparql(self, ctx):
        pass


    # Enter a parse tree produced by SparqlGrammarParser#prefixes.
    def enterPrefixes(self, ctx):
        pass

    # Exit a parse tree produced by SparqlGrammarParser#prefixes.
    def exitPrefixes(self, ctx):
        pass


    # Enter a parse tree produced by SparqlGrammarParser#prefix.
    def enterPrefix(self, ctx):
        pass

    # Exit a parse tree produced by SparqlGrammarParser#prefix.
    def exitPrefix(self, ctx):
        pass


    # Enter a parse tree produced by SparqlGrammarParser#query.
    def enterQuery(self, ctx):
        pass

    # Exit a parse tree produced by SparqlGrammarParser#query.
    def exitQuery(self, ctx):
        pass


    # Enter a parse tree produced by SparqlGrammarParser#projections.
    def enterProjections(self, ctx):
        pass

    # Exit a parse tree produced by SparqlGrammarParser#projections.
    def exitProjections(self, ctx):
        self.elements.projections.append(ctx.VAR())

    # Enter a parse tree produced by SparqlGrammarParser#tripats.
    def enterTripats(self, ctx):
        pass

    # Exit a parse tree produced by SparqlGrammarParser#tripats.
    def exitTripats(self, ctx):
        pass


    # Enter a parse tree produced by SparqlGrammarParser#tripat.
    def enterTripat(self, ctx):
        pass

    # Exit a parse tree produced by SparqlGrammarParser#tripat.
    def exitTripat(self, ctx):
        #subjects
        if ctx.subj().URI() != None:
            s = URI(ctx.subj().URI())
        else:
            s = Variable(ctx.subj().VAR())

        #predicate
        p = URI(ctx.pred().URI())

        #object
        if ctx.obj().URI() != None:
            o = URI(ctx.obj().URI())    
        else:
            o = Variable(ctx.obj().VAR())

        self.elements.tripats.append(Tripat(s, p, o))


    # Enter a parse tree produced by SparqlGrammarParser#subj.
    def enterSubj(self, ctx):
        pass

    # Exit a parse tree produced by SparqlGrammarParser#subj.
    def exitSubj(self, ctx):
        pass


    # Enter a parse tree produced by SparqlGrammarParser#pred.
    def enterPred(self, ctx):
        pass

    # Exit a parse tree produced by SparqlGrammarParser#pred.
    def exitPred(self, ctx):
        pass


    # Enter a parse tree produced by SparqlGrammarParser#obj.
    def enterObj(self, ctx):
        pass

    # Exit a parse tree produced by SparqlGrammarParser#obj.
    def exitObj(self, ctx):
        pass


    # Enter a parse tree produced by SparqlGrammarParser#filters.
    def enterFilters(self, ctx):
        pass

    # Exit a parse tree produced by SparqlGrammarParser#filters.
    def exitFilters(self, ctx):
        pass


