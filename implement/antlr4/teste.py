from antlr4 import *
from SparqlGrammarLexer import SparqlGrammarLexer
from SparqlGrammarParser import SparqlGrammarParser
from SparqlGrammarListener import SparqlGrammarListener
from SparqlTranslator import SparqlTranslator
import timeit
import sys

#sintaxe:
#argv[1]: string SPARQL a ser traduzida
#argv[2]: banco de dado contendo o esquema
def main(argv):
    #database = getDatabase(argv[2])
    #print database
    start = timeit.default_timer()
    input = FileStream(argv[1])
    lexer = SparqlGrammarLexer(input)
    stream = CommonTokenStream(lexer)
    parser = SparqlGrammarParser(stream)
    tree = parser.sparql()
    listener = SparqlGrammarListener()
    walker = ParseTreeWalker()
    walker.walk(listener, tree)
    #print getRowsDb(database, listener.elements.getPredicates())
    translator = SparqlTranslator(listener.elements, argv[2])
    end = timeit.default_timer()

    print end - start

if __name__ == '__main__':
    main(sys.argv)
    
