// Generated from SparqlGrammar.g4 by ANTLR 4.5
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SparqlGrammarParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		SELECT=1, PREFIX=2, WHERE=3, OPECURBRA=4, CLOCURBRA=5, COLON=6, OPEPTBRA=7, 
		CLOPTBRA=8, URI=9, VAR=10, DOT=11, SPACES=12;
	public static final int
		RULE_sparql = 0, RULE_prefixes = 1, RULE_prefix = 2, RULE_query = 3, RULE_projections = 4, 
		RULE_tripats = 5, RULE_tripat = 6, RULE_subj = 7, RULE_pred = 8, RULE_obj = 9, 
		RULE_filters = 10;
	public static final String[] ruleNames = {
		"sparql", "prefixes", "prefix", "query", "projections", "tripats", "tripat", 
		"subj", "pred", "obj", "filters"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'SELECT'", "'PREFIX'", "'WHERE'", "'{'", "'}'", "':'", "'<'", "'>'", 
		null, null, "'.'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "SELECT", "PREFIX", "WHERE", "OPECURBRA", "CLOCURBRA", "COLON", 
		"OPEPTBRA", "CLOPTBRA", "URI", "VAR", "DOT", "SPACES"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SparqlGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SparqlGrammarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class SparqlContext extends ParserRuleContext {
		public PrefixesContext prefixes() {
			return getRuleContext(PrefixesContext.class,0);
		}
		public QueryContext query() {
			return getRuleContext(QueryContext.class,0);
		}
		public SparqlContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sparql; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).enterSparql(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).exitSparql(this);
		}
	}

	public final SparqlContext sparql() throws RecognitionException {
		SparqlContext _localctx = new SparqlContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_sparql);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(22);
			prefixes();
			setState(23);
			query();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrefixesContext extends ParserRuleContext {
		public List<PrefixContext> prefix() {
			return getRuleContexts(PrefixContext.class);
		}
		public PrefixContext prefix(int i) {
			return getRuleContext(PrefixContext.class,i);
		}
		public PrefixesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prefixes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).enterPrefixes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).exitPrefixes(this);
		}
	}

	public final PrefixesContext prefixes() throws RecognitionException {
		PrefixesContext _localctx = new PrefixesContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_prefixes);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==PREFIX) {
				{
				{
				setState(25);
				prefix();
				}
				}
				setState(30);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrefixContext extends ParserRuleContext {
		public TerminalNode PREFIX() { return getToken(SparqlGrammarParser.PREFIX, 0); }
		public TerminalNode URI() { return getToken(SparqlGrammarParser.URI, 0); }
		public TerminalNode COLON() { return getToken(SparqlGrammarParser.COLON, 0); }
		public TerminalNode OPEPTBRA() { return getToken(SparqlGrammarParser.OPEPTBRA, 0); }
		public TripatsContext tripats() {
			return getRuleContext(TripatsContext.class,0);
		}
		public TerminalNode CLOPTBRA() { return getToken(SparqlGrammarParser.CLOPTBRA, 0); }
		public PrefixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prefix; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).enterPrefix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).exitPrefix(this);
		}
	}

	public final PrefixContext prefix() throws RecognitionException {
		PrefixContext _localctx = new PrefixContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_prefix);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(31);
			match(PREFIX);
			setState(32);
			match(URI);
			setState(33);
			match(COLON);
			setState(34);
			match(OPEPTBRA);
			setState(35);
			tripats();
			setState(36);
			match(CLOPTBRA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QueryContext extends ParserRuleContext {
		public TerminalNode SELECT() { return getToken(SparqlGrammarParser.SELECT, 0); }
		public ProjectionsContext projections() {
			return getRuleContext(ProjectionsContext.class,0);
		}
		public FiltersContext filters() {
			return getRuleContext(FiltersContext.class,0);
		}
		public QueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_query; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).enterQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).exitQuery(this);
		}
	}

	public final QueryContext query() throws RecognitionException {
		QueryContext _localctx = new QueryContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_query);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(38);
			match(SELECT);
			setState(39);
			projections();
			setState(40);
			filters();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProjectionsContext extends ParserRuleContext {
		public TerminalNode VAR() { return getToken(SparqlGrammarParser.VAR, 0); }
		public ProjectionsContext projections() {
			return getRuleContext(ProjectionsContext.class,0);
		}
		public ProjectionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_projections; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).enterProjections(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).exitProjections(this);
		}
	}

	public final ProjectionsContext projections() throws RecognitionException {
		ProjectionsContext _localctx = new ProjectionsContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_projections);
		try {
			setState(45);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(42);
				match(VAR);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(43);
				match(VAR);
				setState(44);
				projections();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TripatsContext extends ParserRuleContext {
		public List<TripatContext> tripat() {
			return getRuleContexts(TripatContext.class);
		}
		public TripatContext tripat(int i) {
			return getRuleContext(TripatContext.class,i);
		}
		public TripatsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tripats; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).enterTripats(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).exitTripats(this);
		}
	}

	public final TripatsContext tripats() throws RecognitionException {
		TripatsContext _localctx = new TripatsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_tripats);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(48); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(47);
				tripat();
				}
				}
				setState(50); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==URI || _la==VAR );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TripatContext extends ParserRuleContext {
		public SubjContext subj() {
			return getRuleContext(SubjContext.class,0);
		}
		public PredContext pred() {
			return getRuleContext(PredContext.class,0);
		}
		public ObjContext obj() {
			return getRuleContext(ObjContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SparqlGrammarParser.DOT, 0); }
		public TripatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tripat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).enterTripat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).exitTripat(this);
		}
	}

	public final TripatContext tripat() throws RecognitionException {
		TripatContext _localctx = new TripatContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_tripat);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(52);
			subj();
			setState(53);
			pred();
			setState(54);
			obj();
			setState(55);
			match(DOT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubjContext extends ParserRuleContext {
		public TerminalNode URI() { return getToken(SparqlGrammarParser.URI, 0); }
		public TerminalNode VAR() { return getToken(SparqlGrammarParser.VAR, 0); }
		public SubjContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subj; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).enterSubj(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).exitSubj(this);
		}
	}

	public final SubjContext subj() throws RecognitionException {
		SubjContext _localctx = new SubjContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_subj);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(57);
			_la = _input.LA(1);
			if ( !(_la==URI || _la==VAR) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredContext extends ParserRuleContext {
		public TerminalNode URI() { return getToken(SparqlGrammarParser.URI, 0); }
		public PredContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pred; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).enterPred(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).exitPred(this);
		}
	}

	public final PredContext pred() throws RecognitionException {
		PredContext _localctx = new PredContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_pred);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(59);
			match(URI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjContext extends ParserRuleContext {
		public TerminalNode URI() { return getToken(SparqlGrammarParser.URI, 0); }
		public TerminalNode VAR() { return getToken(SparqlGrammarParser.VAR, 0); }
		public ObjContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_obj; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).enterObj(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).exitObj(this);
		}
	}

	public final ObjContext obj() throws RecognitionException {
		ObjContext _localctx = new ObjContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_obj);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61);
			_la = _input.LA(1);
			if ( !(_la==URI || _la==VAR) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FiltersContext extends ParserRuleContext {
		public TerminalNode WHERE() { return getToken(SparqlGrammarParser.WHERE, 0); }
		public TerminalNode OPECURBRA() { return getToken(SparqlGrammarParser.OPECURBRA, 0); }
		public TripatsContext tripats() {
			return getRuleContext(TripatsContext.class,0);
		}
		public TerminalNode CLOCURBRA() { return getToken(SparqlGrammarParser.CLOCURBRA, 0); }
		public FiltersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).enterFilters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlGrammarListener ) ((SparqlGrammarListener)listener).exitFilters(this);
		}
	}

	public final FiltersContext filters() throws RecognitionException {
		FiltersContext _localctx = new FiltersContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_filters);
		try {
			setState(69);
			switch (_input.LA(1)) {
			case EOF:
				enterOuterAlt(_localctx, 1);
				{
				}
				break;
			case WHERE:
				enterOuterAlt(_localctx, 2);
				{
				setState(64);
				match(WHERE);
				setState(65);
				match(OPECURBRA);
				setState(66);
				tripats();
				setState(67);
				match(CLOCURBRA);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\16J\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4"+
		"\f\t\f\3\2\3\2\3\2\3\3\7\3\35\n\3\f\3\16\3 \13\3\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\5\6\60\n\6\3\7\6\7\63\n\7\r\7\16\7"+
		"\64\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\5\fH\n\f\3\f\2\2\r\2\4\6\b\n\f\16\20\22\24\26\2\3\3\2\13\fB\2\30"+
		"\3\2\2\2\4\36\3\2\2\2\6!\3\2\2\2\b(\3\2\2\2\n/\3\2\2\2\f\62\3\2\2\2\16"+
		"\66\3\2\2\2\20;\3\2\2\2\22=\3\2\2\2\24?\3\2\2\2\26G\3\2\2\2\30\31\5\4"+
		"\3\2\31\32\5\b\5\2\32\3\3\2\2\2\33\35\5\6\4\2\34\33\3\2\2\2\35 \3\2\2"+
		"\2\36\34\3\2\2\2\36\37\3\2\2\2\37\5\3\2\2\2 \36\3\2\2\2!\"\7\4\2\2\"#"+
		"\7\13\2\2#$\7\b\2\2$%\7\t\2\2%&\5\f\7\2&\'\7\n\2\2\'\7\3\2\2\2()\7\3\2"+
		"\2)*\5\n\6\2*+\5\26\f\2+\t\3\2\2\2,\60\7\f\2\2-.\7\f\2\2.\60\5\n\6\2/"+
		",\3\2\2\2/-\3\2\2\2\60\13\3\2\2\2\61\63\5\16\b\2\62\61\3\2\2\2\63\64\3"+
		"\2\2\2\64\62\3\2\2\2\64\65\3\2\2\2\65\r\3\2\2\2\66\67\5\20\t\2\678\5\22"+
		"\n\289\5\24\13\29:\7\r\2\2:\17\3\2\2\2;<\t\2\2\2<\21\3\2\2\2=>\7\13\2"+
		"\2>\23\3\2\2\2?@\t\2\2\2@\25\3\2\2\2AH\3\2\2\2BC\7\5\2\2CD\7\6\2\2DE\5"+
		"\f\7\2EF\7\7\2\2FH\3\2\2\2GA\3\2\2\2GB\3\2\2\2H\27\3\2\2\2\6\36/\64G";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}