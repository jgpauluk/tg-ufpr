# Generated from java-escape by ANTLR 4.5
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO


def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2")
        buf.write(u"\16J\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write(u"\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t")
        buf.write(u"\r\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3")
        buf.write(u"\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3")
        buf.write(u"\7\3\b\3\b\3\t\3\t\3\n\6\n;\n\n\r\n\16\n<\3\13\3\13\3")
        buf.write(u"\13\3\f\3\f\3\r\6\rE\n\r\r\r\16\rF\3\r\3\r\2\2\16\3\3")
        buf.write(u"\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16")
        buf.write(u"\3\2\5\5\2C\\aac|\4\2C\\c|\4\2\13\f\"\"K\2\3\3\2\2\2")
        buf.write(u"\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r")
        buf.write(u"\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25")
        buf.write(u"\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\3\33\3\2\2\2\5\"\3")
        buf.write(u"\2\2\2\7)\3\2\2\2\t/\3\2\2\2\13\61\3\2\2\2\r\63\3\2\2")
        buf.write(u"\2\17\65\3\2\2\2\21\67\3\2\2\2\23:\3\2\2\2\25>\3\2\2")
        buf.write(u"\2\27A\3\2\2\2\31D\3\2\2\2\33\34\7U\2\2\34\35\7G\2\2")
        buf.write(u"\35\36\7N\2\2\36\37\7G\2\2\37 \7E\2\2 !\7V\2\2!\4\3\2")
        buf.write(u"\2\2\"#\7R\2\2#$\7T\2\2$%\7G\2\2%&\7H\2\2&\'\7K\2\2\'")
        buf.write(u"(\7Z\2\2(\6\3\2\2\2)*\7Y\2\2*+\7J\2\2+,\7G\2\2,-\7T\2")
        buf.write(u"\2-.\7G\2\2.\b\3\2\2\2/\60\7}\2\2\60\n\3\2\2\2\61\62")
        buf.write(u"\7\177\2\2\62\f\3\2\2\2\63\64\7<\2\2\64\16\3\2\2\2\65")
        buf.write(u"\66\7>\2\2\66\20\3\2\2\2\678\7@\2\28\22\3\2\2\29;\t\2")
        buf.write(u"\2\2:9\3\2\2\2;<\3\2\2\2<:\3\2\2\2<=\3\2\2\2=\24\3\2")
        buf.write(u"\2\2>?\7A\2\2?@\t\3\2\2@\26\3\2\2\2AB\7\60\2\2B\30\3")
        buf.write(u"\2\2\2CE\t\4\2\2DC\3\2\2\2EF\3\2\2\2FD\3\2\2\2FG\3\2")
        buf.write(u"\2\2GH\3\2\2\2HI\b\r\2\2I\32\3\2\2\2\5\2<F\3\b\2\2")
        return buf.getvalue()


class SparqlGrammarLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]


    SELECT = 1
    PREFIX = 2
    WHERE = 3
    OPECURBRA = 4
    CLOCURBRA = 5
    COLON = 6
    OPEPTBRA = 7
    CLOPTBRA = 8
    URI = 9
    VAR = 10
    DOT = 11
    SPACES = 12

    modeNames = [ u"DEFAULT_MODE" ]

    literalNames = [ u"<INVALID>",
            u"'SELECT'", u"'PREFIX'", u"'WHERE'", u"'{'", u"'}'", u"':'", 
            u"'<'", u"'>'", u"'.'" ]

    symbolicNames = [ u"<INVALID>",
            u"SELECT", u"PREFIX", u"WHERE", u"OPECURBRA", u"CLOCURBRA", 
            u"COLON", u"OPEPTBRA", u"CLOPTBRA", u"URI", u"VAR", u"DOT", 
            u"SPACES" ]

    ruleNames = [ u"SELECT", u"PREFIX", u"WHERE", u"OPECURBRA", u"CLOCURBRA", 
                  u"COLON", u"OPEPTBRA", u"CLOPTBRA", u"URI", u"VAR", u"DOT", 
                  u"SPACES" ]

    grammarFileName = u"SparqlGrammar.g4"

    def __init__(self, input=None):
        super(SparqlGrammarLexer, self).__init__(input)
        self.checkVersion("4.5")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


