
import Elements
import random
import string
import connectDb
import sys

#############################################
## Conexao com o banco de dados
#############################################

#inteface com banco de dados
def getDatabase(arq):
    db = list()
    for line in file(arq, 'r'):
        db.append(line.split(' '))
    return db

#passa como dados uma lista de predicados
def getRowsDb(db, preds):
    rs = list()
    qry = 'SELECT CS_IDENTIFIER, PROPERTY, VALUETYPE, TABLENAME, TABLEATTRIBUTE FROM TB_DatabaseSchema WHERE PROPERTY IN ('
    for p in preds:
        qry += '\'' + p + '\''
        if(preds.index(p) != len(preds) - 1):
            qry += ', '
    qry += ');'
    #print qry
    return connectDb.ConnectDb.executeQuery(qry)

##############################################
## Estrutura de dados utilizados na traducao
##############################################
class SparqlTranslator():
    #elems:Elements.Elements
    #variables: list<AlphaStar>
    def __init__(self, elems, db):
        self.elements = elems
        self.variables = list()
        self.varGuidMap = dict()
        self.varBind = list()
        self.filters = dict()
        self.ooFilter = list()
        self.soFilter = list()

        self.findStarPatterns(elems)
        for var in self.variables:
            var.fillStList(db)
        #Inicio da aplicacao de filtros s-s, s-o, o-o
        #self.printStarPatterns()
        self.applySubjSubjFilter()
        self.applyObjSubjFilter()
        self.applyObjObjFilter()
        #self.printStarPatterns()
        self.getSQLQuery()


    def getNmAtr(self, nmVar):
        #print 'iniciando getNmAtr'
        for var in self.variables:
            #print var.var, nmVar
            if str(var.var) == str(nmVar):
                if var.nmTable.startswith('Overflow'):
                    return var.nmTable + '.subj'
                else:
                    return var.nmTable + '.OID'
            for tp in var.tps:
                if str(tp.obj.value) == str(nmVar):
                    return var.nmTable + '.' + var.getPredGUID()[str(tp.pred.value)]
        print 'ERRO: Atr nao encontrado'

    def getProjection(self):
        projection = list()
        for proj in self.elements.projections:
            projection.append(str(self.getNmAtr(proj)) + ' AS ' + str(proj)[1:]) 
            #print 'proj', proj
        return projection

    def getSQLQuery(self):
        slc = 'SELECT\n '
        for proj in self.getProjection():
            slc += proj
            if self.getProjection().index(proj) != len(self.getProjection()) -1:
                slc += ','
            slc += '\n'
        slc += ' FROM\n '
        for var in self.variables:
            slc += var.getSQL()
            if self.variables.index(var) != len(self.variables) -1:
                slc += ', '
        where = ''
        for f in self.ooFilter:
            if len(where) != 0:
                where += ' AND '
            where += f
        for f in self.soFilter:
                    #insere condicao
            if len(where) != 0:
                where += ' AND '
            where += f
       
        slc += ' WHERE ' + where #+ ';'
        print slc #+ ' LIMIT 10;'
        return slc
        #print 'query: ', self.ooFilter, self.soFilter, self.getProjection()
    #Funcao que retorna o GUID dado uma variavel
    ##nmVar: string - nome da variavel
    def getVarGUID(self, nmVar):
        if nmVar not in self.varGuidMap:
            guid = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
            self.varGuidMap.update({nmVar : guid})
            return guid
        return self.varGuidMap[nmVar]

    ##############################################################
    ## APLICACAO DE FILTROS
    ##############################################################
    def applyObjObjFilter(self):
        for var in self.variables:
            rmStList = list()
            for st in var.stList:
                for sti in st.stInfo:
                    if isinstance(sti.dsVar, Elements.Variable) and not self.existObjectType(var, sti.dsVar.value, sti.tpObj, var, sti.dsPred):
                        rmStList.append(st)
                        print 'A[o-o]: Nao foi encontrado uma variavel ', sti.dsVar.value, ' do tipo', sti.tpObj
                        sys.exit(-3)

            #Remove sujeitos que nao possuem ligacao objeto - objeto
            var.stList = [x for x in var.stList if x not in rmStList]
            if len(var.stList) == 0:
                print 'E[o-o]: Erro ao tentar encontrar juncao para a variavel ', var.var
        
    #Funcao para verificar se existe um objeto do tipo passado
    #Por padrao, 'nao eh necessario' fazer uma juncao, mas se for encontrado alguma objeto que for variavel com o mesmo nome, mas com tipo diferente, entao deve existir juncao
    ##subjVar: SubjectType
    ##nmVar:string - nome da variavel
    ##tpObj: string - tipo do objeto
    ##primVar utilizado para fazer conexao obj-obj
    def existObjectType(self, subjVar, nmVar, tpObj, primVar, primDsPred):
        rs = True
        for var in self.variables:
            if var == subjVar:
                continue
            for st in var.stList:
                for sti in st.stInfo:
                    if isinstance(sti.dsVar, Elements.Variable):
                        if sti.dsVar.value == nmVar and sti.tpObj == tpObj:
                            #adiciona ligacao - auxiliar na criacao do sql
                            self.ooFilter.append(var.getPredGUID()[sti.dsPred] + ' = ' + primVar.getPredGUID()[primDsPred])
                            #self.varBind.append()
                            return True
                        elif sti.dsVar.value == nmVar and sti.tpObj != tpObj:
                            rs = False
        return rs

    def applySubjSubjFilter(self):
        #percorre as variaveis
        for var in self.variables:
            var.applySubjSubjFilter()
            
    def applyObjSubjFilter(self):
        #percorre todos os SubjectTypeInfo. Se objeto for variavel, verifica se existe uma variavel com este tipo
        for var in self.variables:
            rmStList = list()
            for st in var.stList:
                for sti in st.stInfo:
                    if isinstance(sti.dsVar, Elements.Variable) and not self.existVariableType(sti.dsVar.value, sti.tpObj, var.getPredGUID(), sti.dsPred):
                        print 'A[s-o]: Nao foi encontrado uma variavel ', var.var, ' do tipo ', st.type, '--', sti.dsVar.value, sti.tpObj
                        rmStList.append(st)
                        break

            #Remove os tipos de sujeitos que nao possuem ligacao objeto - objeto
            var.stList = [x for x in var.stList if x not in rmStList]
            if len(var.stList) == 0:
                print 'E[s-o]: Nao foi encontrado tabelas que permita a juncao s-o para a variavel ', var.var
                sys.exit(-2)
    
    #Funcao utilizada para verificar se existe uma varivel dado um nome(i.e., ?a, ?c) e um tipo
    ##varName: string - nome da variavel
    ##varType:string - tipo da variavel. Retornado pelo banco.
    ##dsPred: string - descricao do predicado - utilizado para fazer as conexoes obj - suj
    def existVariableType(self, varName, varType, predGUID, dsPred):
        var = self.getVariable(varName)
        #Se nao existe variavel com este nome no sujeito, nao eh necessario fazer juncao
        if var is None:
            return True
        for st in var.stList:
            if st.type == varType:
                self.soFilter.append(var.nmTable + '.OID = ' + predGUID[dsPred])
                return True
        return False
        

    #elems:Elements
    #returns: list<AlphaStar>
    def findStarPatterns(self, elems):
        #procura no bd pelos predicados
        #para cada tp, busca na maptable e preenche estruturas
        for tp in elems.tripats:
            isTerm = isinstance(tp.subj, Elements.URI)
            var = self.getVariable(tp.subj.value, isTerm)
            if var is None:
                var = AlphaStar(tp.subj.value, isTerm)
                #adiciona na lista de variaveis
                self.variables.append(var)
            #insere tripla na lista de triplas do Alpha
            var.tps.append(tp)
            #insere AlphaStar na lista de variaveis
        pass

    #Procura por uma variavel com o nome name em self.variables
    #name:string - nome do subjeito
    #term:boolean - o sujeito eh um termo e nao variavel?
    #returns: AlphaStar, se encontrar. None, caso contrario
    def getVariable(self, name, term = False):
        for var in self.variables:
            if term and var.term:
                return var
            elif var.var == name:
                return var

        return None
        
    def printStarPatterns(self):
        for var in self.variables:
            print 'tripats para ', var.var
            for tp in var.tps:
                tp.printTripat()
                   
            for st in var.stList:
                print 'grupo de tipo de sujeito:', st.type
                for sti in st.stInfo:
                    print sti.tpSubj, ' ', sti.dsPred, ' ',  sti.tpObj, ' ', sti.dsVar.value, ' ', sti.nmTable, ' ', sti.nmAtr
            print '\n'

#classe que define um padrao de estrela
##var: string - nome da variavel, e.g. ?a
##term:boolean - se sujeito for um termo, insere num grupo especial
##tps: list<Tripat> - lista contendo os padroes de triplas
##stList : list<SubjectType> - conjunto de informacoes sobre uma variavel
class AlphaStar:
    def __init__(self, v, term = False):
        self.var = v
        self.term = term
        self.tps = list()
        self.stList = list()
        self.predGUID = None
        self.nmTable = v[1:]

    def getSQL(self):
        qry = ''
        for sti in self.stList:
            if self.stList.index(sti) > 0:
                qry += ' UNION ALL '
            qry += sti.getSQL(self.getPredGUID())
        return '(' + qry + ') ' + self.nmTable + ' '


    #Retorna os GUID relacionado a um predicado
    def getPredGUID(self):
        if self.predGUID is None:
            self.predGUID = dict()
            self.predGUID.update({'id': 'id'})
            for pred in self.getPredicates():
                guid = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
                self.predGUID.update( {pred : pred + guid })
        return self.predGUID

    #aplica o filtro sujeito - sujeito 
    def applySubjSubjFilter(self):
        rmList = list()
        for st in self.stList:
            for tp in self.tps:
                sti = st.getStInfo(tp.pred.value)
                #Nao existe este predicado para este tipo de variavel
                if sti is None:
                    rmList.append(st)
                    #print st.type, ' foi removido por nao possuir o pred ' + tp.pred.value
                    break
                #print 'predicado encontrado ',  sti.dsPred, 'no grupo ', st.type
                #se existe, preenche o objeto
                #sti.setDsObj = tp.obj
        if len(self.stList) == len(rmList):
            print 'Erro durante a juncao sujeito - sujeito: nao foi possivel encontrar tabelas para a variavel ' + self.var + 'dos predicados ', self.getPredicates()
            sys.exit(-1)
        self.stList = [x for x in self.stList if x not in rmList]
        #dself.stList.removeAll(rmList)
        

    #preenche os SubjectType, buscando as info no banco de dados
    ##bd:string - contem o nome do arquivo de banco de dados
    def fillStList(self, bd):

        ##TODO: Coloca o ConnectDb aqui!
        for row in getRowsDb(getDatabase(bd), self.getPredicates()):
            sti = SubjectTypeInfo(row)
            sti.setDsVar(self.findVariable(sti.dsPred))
            st = self.findSubjectType(sti.tpSubj)
            if st is None:
                st = SubjectType(sti.tpSubj)
                self.stList.append(st)
            st.stInfo.append(sti)

    #encontra um SubjectType em stList
    ##tpe:string - tipo do subject (i.e., cs1, cs2, ...)
    ##returns: SubjectType ou None se nao encontrar
    def findSubjectType(self, tpe):
        for st in self.stList:
            if st.type == tpe:
                return st
        return None

    #encontra a variavel relacionada ao predicado
    ##pred:string - URI do predicado
    ##return: Variable ou URI, se encontrou. None se for literal
    def findVariable(self, pred):
        for tp in self.tps:
            if tp.pred.value == pred:
                return tp.obj
        return None

    def getPredicates(self):
        preds = list()
        for tp in self.tps:
            preds.append(tp.pred.value)

        return preds

#classe que define informacoes sobre determinada tipo de variavel
#obs.: para cada instancia, ela deva possuir todos os predicados do star pattern
##type:string - tipo da variavel(nas anotacoes seria cs1, cs2, cs3, ...)
##stInfo:list<SubjectTypeInfo> - lista com informacoes de uma determinada variavel, cada item corresponde a uma informacao de um predicado. No final, para cada predicado, devera possuir um correspondete do SubjectTypeInfo
class SubjectType:
    def __init__(self, t):
        self.type = t
        self.stInfo = list()

    #Procura um SubjectTypeInfo na lista stInfo a partir de um predicado
    #pred:string - string contendo o predicado
    #return: SubjectTypeInfo, se encontrar
    #        caso contrario
    def getStInfo(self, pred):
        for sti in self.stInfo:
            if sti.dsPred == pred:
                return sti
        return None
    
    ##predGUID: recebe o predGUID do alphaStar
    def getSQL(self, predGUID):
        sct = 'select '
        proj = ''
        where = ''
        primId = ''
        table = list()
        mainTable = ''
        for sti in self.stInfo:
            # Sempre projeta a OID da tabela, exceto se for Overflow
            if sti.nmTable not in table:
                if 'Overflow' in sti.nmTable:
                    sbj = '.subj'
                else:
                    sbj = '.OID'

                # Verifica se a tabela eh a principal.
                # TODO: recupera a informacao baseado no parametro 'OID' fornecido pelo Rafael
                if not ('Overflow' in table or 'MultivalueRDF' in table):
                    mainTable = sti.nmTable

                # Se ainda nao projetou nenhuma tabela, guarda o primeiro sujeito para fazer a juncao com as demais tabelas.
                if len(table) == 0:
                    proj += sti.nmTable + sbj
                    primId = sti.nmTable + sbj
                else:
                    if len(where) > 0:
                        where += ' AND '
                    where += sti.nmTable + sbj + ' = ' + primId
                table.append(sti.nmTable)
            proj += ', '
            proj += sti.nmTable + '.' + sti.nmAtr + ' AS ' + predGUID[sti.dsPred]

            #Eh somente AND, entao sao tudo not null
            if len(where) > 0:
                where += ' AND '
            # TODO: Adicionado no WHERE o pred = PREDICADO, se Overflow
            where += sti.nmTable + '.' + sti.nmAtr + ' IS NOT NULL'
        sct = 'SELECT ' + proj + ' FROM '
        for tb in table:
            if table.index(tb) > 0:
                sct += ', '
            sct += tb
        if len(where) > 0:
            sct += ' WHERE ' + where

        if len(mainTable) > 0:
            sct = sct + ' UNION ALL ' + sct.replace(mainTable, mainTable + '_View')
        return sct


#classe que define quais aonde estao armazenados os predicados. Estah ligado a um SubjectType
##tpSubj: string
##tpObj:string
##varObj:string
##nmTable:string
##nmAtr:string
class SubjectTypeInfo:
    def __init__(self, tpSubj, tpObj, varObj, nmTable, nmAtr):
        self.tpSubj = tpSubj
        self.tpObj = tpObj
        self.varObj = varObj
        self.nmTable = nmTable
        self.nmAtr = nmAtr
    
    #recebe uma linha do banco de dados e cria um SubjectTypeInfo
    ##row: list<string> - lista retornada pelo banco de dados
    def __init__(self, row):
        self.tpSubj = row['CS_IDENTIFIER']
        self.dsPred = row['PROPERTY']
        self.tpObj = row['VALUETYPE']
        self.nmTable = row['TABLENAME']
        self.nmAtr = row['TABLEATTRIBUTE']

    #define o nome da variavel para o SubjectTypeInfo
    ##var:string - nome da variavel(e.g., ?a, ?b, ...)
    def setDsVar(self, var):
        self.dsVar = var
