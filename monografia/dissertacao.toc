\select@language {brazil}
\contentsline {chapter}{Resumo}{i}{chapter*.1}
\contentsline {chapter}{Lista de Figuras}{vi}{chapter*.3}
\contentsline {chapter}{Lista de Tabelas}{vii}{chapter*.4}
\contentsline {chapter}{Lista de Algoritmos}{ix}{chapter*.5}
\contentsline {chapter}{\numberline {1}Introdu\c c\~ao}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}RDF}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Resource Description Framework}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}SPARQL Protocol And RDF Query Language}{8}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}SPARQL Query Language}{9}{subsection.2.2.1}
\contentsline {chapter}{\numberline {3}Mapeamento RDF para Relacional}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Emergent Relational Schema}{11}{section.3.1}
\contentsline {section}{\numberline {3.2}Tradu\c c\~ao SPARQL para SQL}{14}{section.3.2}
\contentsline {section}{\numberline {3.3}Discuss\~oes}{16}{section.3.3}
\contentsline {chapter}{\numberline {4}AORR: Um Backend Relacional para RDF}{17}{chapter.4}
\contentsline {section}{\numberline {4.1}M\'odulo de extra\c c\~ao de estrutura e armazenamento}{18}{section.4.1}
\contentsline {section}{\numberline {4.2}M\'odulo de tradu\c c\~ao}{20}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Procura por padr\~oes estrela}{23}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Filtragem por liga\c c\~oes}{24}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Montagem SQL}{25}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Discuss\~oes}{27}{section.4.3}
\contentsline {chapter}{\numberline {5}Implementa\c c\~ao}{29}{chapter.5}
\contentsline {section}{\numberline {5.1}Tecnologias utilizadas}{29}{section.5.1}
\contentsline {section}{\numberline {5.2}Experimentos}{30}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Ambiente de testes}{30}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Testes e resultados}{32}{subsection.5.2.2}
\contentsline {section}{\numberline {5.3}Discuss\~oes}{33}{section.5.3}
\contentsline {chapter}{\numberline {6}Conclus\~oes}{37}{chapter.6}
\contentsline {section}{\numberline {6.1}Trabalhos Futuros}{38}{section.6.1}
\contentsline {chapter}{Refer\^encias Bibliogr\'aficas}{40}{chapter*.32}
\contentsline {chapter}{\numberline {A}Consultas utilizadas nos experimentos}{41}{appendix.A}
\contentsline {section}{\numberline {A.1}Consulta C1}{41}{section.A.1}
\contentsline {section}{\numberline {A.2}Consulta C2}{42}{section.A.2}
\contentsline {section}{\numberline {A.3}Consulta C3}{43}{section.A.3}
\contentsline {section}{\numberline {A.4}Consulta C4}{44}{section.A.4}
\contentsline {section}{\numberline {A.5}Consulta C5}{45}{section.A.5}
