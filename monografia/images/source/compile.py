#!/usr/bin/python

import subprocess, sys

commands = [
    ['pdflatex', sys.argv[1] + '.tex'],
    ['bibtex', sys.argv[1] + '.aux'],
    ['pdflatex', sys.argv[1] + '.tex'],
    ['pdflatex', sys.argv[1] + '.tex'],
    ['rm', sys.argv[1] + '.aux'],
    ['rm', sys.argv[1] + '.log'],
    ['rm', sys.argv[1] + '.blg'],
    ['rm', sys.argv[1] + '.bbl']
]

for c in commands:
    subprocess.call(c)
